import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from multiprocessing import Pool

import backup_strategies_eval as ta

col_names = ['cycle', 'op', 'width', 'addr']
block_sizes = [1, 8, 128]

def add_poisson_intervals(df, p=1e-6):
    '''Add intervals considering power failures distributed as a poisson'''
    #p = 1e-6 : 1 prw fail every 1M clock cycles 
    prog_cycles = df.cycle.iloc[-1] #df.cycle.max() 
    events = np.random.choice((0, 1), size=prog_cycles, replace=True, p=[1-p, p])
    pwr_fail_times = np.where(events == 1)[0]
    df['interval'] = 0
    for t in pwr_fail_times:
        df['interval'] += t < df.cycle
    return len(pwr_fail_times)+1

def add_random_intervals(df):
    t = 0
    i = 0
    df['interval'] = 0
    while t < df.cycle.max():
        iv_size = np.random.randint(1e5, 1e7)
        df['interval'] = df.interval.mask((df.cycle > t) & (df.cycle < t+iv_size), i)
        i += 1
        t += iv_size

    return i

def run_with_random_intervals(fname, times=100):
    df = pd.read_csv(fname, names=ta.col_names)

    ta.add_address_blocks(df, block_sizes)
    full_mem_page_512 = int(len(df.block_128.unique()) * 128)

    run_df = pd.DataFrame(columns=['backup_size', 'num_intervals'])

    for i in range(times):
        n = add_poisson_intervals(df)
        mod8 = ta.count_st_once_per_interval(df, 'interval', 'block_8') * 8 
        mod8_avg = mod8.mean() #((1-mod8/full_mem_page_512)).mean()
        run_df.loc[i] = {'backup_size': mod8_avg, 'num_intervals': n}        

    stats = {
        'full_mem_page512' : full_mem_page_512,
        'savings_avg': (1 - (run_df.backup_size / full_mem_page_512)).mean(), 
        'savings_std': (1 - (run_df.backup_size / full_mem_page_512)).std(),
        'num_interval_avg': run_df.num_intervals.mean(),
        'num_interval_std': run_df.num_intervals.std(),
        }
    return fname, stats

def main():
    np.random.seed(0)
    cols = ['full_mem_page512', 'savings_avg', 'savings_std', 'num_interval_avg', 'num_interval_std']
    comp_df = pd.DataFrame(columns=cols)

    trace_files = [f'./script_traces/nocache/{f}' for f in os.listdir('./script_traces/nocache/')]
     
    with Pool() as pool:
        for d in pool.map(run_with_random_intervals, trace_files):
            comp_df.loc[d[0]] = d[1]
    comp_df.loc['average'] = comp_df.mean()
    print(comp_df)

    comp_df.to_csv('poisson_1e-6_iv_100.csv')

if __name__ == '__main__':
    main()
