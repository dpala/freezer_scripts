import os
import numpy as np
import pandas as pd
from multiprocessing import Pool
from functools import partial

from pint import UnitRegistry

import backup_strategies_eval as ta

ureg = UnitRegistry()
Q_ = ureg.Quantity 

def run(name, freq, sram_lut, nvms):
    col_names = ['cycle', 'op', 'width', 'addr']
    block_sizes = [1, 8]

    df = pd.read_csv(f'../traces/{name}', names=ta.col_names)

    ta.add_address_blocks(df, block_sizes)
    mem_pwr_2 = int(2 ** np.ceil(np.log2(df.block_1.nunique())))

    mem_size = int(mem_pwr_2 * 4) # memory size in bytes

    if mem_size not in sram_lut.index:
        print(f'[{name}]: memory size ({mem_size}) not in [{sram_lut.index}]')
        return None 

    ops = df.op.value_counts() # give back number of LD and ST
    pwr_fail_per_cycle = 1e-6
    prog_cycles = df.cycle.iloc[-1]

    E_sram_r = sram_lut.loc[mem_size]['Read Energy']
    E_sram_w = sram_lut.loc[mem_size]['Write Energy']
    P_sram_leak = sram_lut.loc[mem_size]['Leakage']

    E_sram_prog = E_sram_w * ops['ST'] + E_sram_r * ops['LD']

    df['interval'] = np.array(np.round(df.cycle * pwr_fail_per_cycle), dtype=int)
    n = df.interval.nunique() # number of intervals

    modified_in_interval = ta.count_st_once_per_interval(df, 'interval', 'block_8') * 8 

    res = {
        'freezer_avg_backup_size': modified_in_interval.mean(), 
        'freezer_tot_backup_size': modified_in_interval.sum(), 
        'num_intervals': n,
        'mem_size': int(mem_pwr_2), 
        'E_program': E_sram_prog,
        'E_leakage_sram': P_sram_leak * prog_cycles / freq,
    }

    for nvm, lut in nvms.items():
        E_nvm_r = lut.loc[mem_size]['Read Energy'] 
        E_nvm_w = lut.loc[mem_size]['Write Energy'] 
        P_nvm_leak = lut.loc[mem_size]['Leakage'] 

        E_save = E_sram_r + E_nvm_w 
        E_restore = mem_pwr_2 * (E_nvm_r + E_sram_w)
        E_full_backup = mem_pwr_2 * E_save 

        E_nvm_tot = E_nvm_w * ops['ST'] + E_nvm_r * ops['LD']
        E_freezer_tot = E_sram_prog + E_restore * n + E_save * modified_in_interval.sum()
        E_full_mem_tot = E_sram_prog + n * (E_restore + E_full_backup) 
        
        res[f'E_freezer_{nvm}'] = E_freezer_tot.to('mJ')
        res[f'E_full_memory_{nvm}'] = E_full_mem_tot.to('mJ')
        res[f'E_leakage_{nvm}'] = P_nvm_leak * prog_cycles / freq 
        res[f'E_{nvm}_only'] = E_nvm_tot.to('mJ')

    return name, res

def build_luts():
    sram_lut = pd.read_csv('../nvsim_res/sram_st_c28soi.csv', skipinitialspace=True)
    sram_lut = sram_lut[['Capacity', 'Read Latency', 'Write Latency', 'Read Energy', 'Write Energy', 'Leakage']]
    sram_lut['Read Energy'] = sram_lut['Read Energy'].apply(lambda e: Q_(e))
    sram_lut['Write Energy'] = sram_lut['Write Energy'].apply(lambda e: Q_(e))
    sram_lut['Read Latency'] = sram_lut['Read Latency'].apply(lambda e: Q_(e))
    sram_lut['Write Latency'] = sram_lut['Write Latency'].apply(lambda e: Q_(e))
    sram_lut['Leakage'] = sram_lut['Leakage'].apply(lambda e: Q_(e))
    sram_lut.Capacity = sram_lut.Capacity.apply(lambda e: int(Q_(e).to('B').magnitude))
    sram_lut.set_index('Capacity', inplace=True)
    #print(sram_lut[['Read Latency', 'Write Latency', 'Read Energy', 'Write Energy']].applymap(Q_))
    nvms = {
        'stt': pd.read_csv('../nvsim_res/stt.csv', skipinitialspace=True),
        'stt_aggressive': pd.read_csv('../nvsim_res/stt_aggressive.csv', skipinitialspace=True),
        'rram': pd.read_csv('../nvsim_res/rram.csv', skipinitialspace=True),
    }

    for nvm, df in nvms.items():
        df = df[['Capacity', 'Read Latency', 'Write Latency', 'Read Energy', 'Write Energy', 'Leakage']]
        df['Read Energy'] = df['Read Energy'].apply(lambda e: Q_(e))
        df['Write Energy'] = df['Write Energy'].apply(lambda e: Q_(e))
        df['Read Latency'] = df['Read Latency'].apply(lambda e: Q_(e))
        df['Write Latency'] = df['Write Latency'].apply(lambda e: Q_(e))
        df['Leakage'] = df['Leakage'].apply(lambda e: Q_(e))
        df.Capacity = df.Capacity.apply(lambda e: int(Q_(e).to('B').magnitude))
        df.set_index('Capacity', inplace=True)
        nvms[nvm] = df

    return sram_lut, nvms

def main():
    sram_lut, nvms = build_luts() 
    comp_df = None 
    run_fn = partial(run, freq=Q_('20MHz'), sram_lut=sram_lut, nvms=nvms) 
    #d = run_fn('fft.csv')
    #if d is not None:
    #    comp_df = pd.DataFrame(d[1], index=[d[0]])
    #print(d[1])
    with Pool() as pool:
        for d in pool.map(run_fn, os.listdir('../traces/')):
            if d is None:
                continue
            if comp_df is None:
                comp_df = pd.DataFrame(d[1], index=[d[0]])
            else:
                comp_df.loc[d[0]] = d[1]
    comp_df.mem_size = np.int32(comp_df.mem_size)
    comp_df.loc['average'] = comp_df.mean()

    print(comp_df)

    comp_df.to_csv('energy_1e-6iv_units.csv')
    tmp = comp_df[['mem_size', 'E_freezer_stt_aggressive', 'E_freezer_rram', 'E_leakage_sram', 'E_leakage_stt_aggressive', 'E_leakage_rram']].iloc[:-1]
    for c in ['E_freezer_stt_aggressive', 'E_freezer_rram', 'E_leakage_sram', 'E_leakage_stt_aggressive', 'E_leakage_rram']:
        tmp[c] = tmp[c].apply(lambda e: '{:.3~P}'.format(e.to('mJ')))

    tmp.to_csv('table.csv')

    print(tmp)
    with open('table.tex', 'w') as f:
        f.write(tmp.to_latex())
    print(tmp.to_latex())

if __name__ == '__main__':
    main()
