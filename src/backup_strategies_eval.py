import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#plt.rcParams['figure.figsize'] = [15,10]#[9.5, 6]

col_names = ['cycle', 'op', 'width', 'addr']

trace_files = {
    'fft_8_128' : './script_traces/fft_8_128.nocache.trace.csv',
    #'fft_32_128': './script_traces/fft_32_128.nocache.trace.csv',
    #'qsort_small' : './script_traces/qsort_small.trace.csv',
    #'susan_edge': './script_traces/susan_small.nocache.edge.trace.csv',
    #'susan_smooth': './script_traces/susan_small.nocache.smooth.trace.csv',
    #'susan_large_edge': './script_traces/susan_large.nocache.edge.trace.csv',
    ##'susan_large_smooth': './script_traces/susan_large.nocache.smooth.trace.csv',
    ##'search_small': './script_traces/search_small.nocache.trace.csv',
    #'search_large': './script_traces/search_large.nocache.trace.csv',
    #'dijkstra_small': './script_traces/dijkstra_small.nocache.trace.csv',
    #'crc_large': './script_traces/crc.nocache.large.trace.csv',
    #'matmul_int_16': './script_traces/matmul_int_16.nocache.trace.csv',
    #'matmul_int_32': './script_traces/matmul_int_32.nocache.trace.csv',
    #'matmul_float_16': './script_traces/matmul_float_16.nocache.trace.csv',
}

def count_last_use_alive(g):
    return g.loc[g.block_1.drop_duplicates(keep='last').index].oracle_alive.sum()

def add_address_blocks(df, block_sizes):
    for bs in block_sizes:
        shift_val = int(np.log2(bs*4)) 
        df[f'block_{bs}'] = np.right_shift(df.addr, shift_val)

def add_intervals(df, intervals):
    for cs in intervals:
        df[f'interval_{cs}'] = np.array(np.round(df.cycle / 10**cs), dtype=int)

def count_once_per_interval(df, interval, block):
    return df[[interval, block]].drop_duplicates().groupby(interval).size()

def count_st_once_per_interval(df, interval, block):
    by_interval = df[[block, 'op', interval]].groupby(interval)
    return by_interval.apply(lambda g: g.loc[g.op == 'ST', block].nunique())

def analyse_trace(tr):
   print(f'Reading dataframe for trace: ./script_traces/nocache/{tr}')
   df = pd.read_csv(f'./script_traces/nocache/{tr}', names=col_names)
   comp_df = pd.DataFrame() 

   add_address_blocks(df, block_sizes)
   add_intervals(df, [6,7])    

   # ORACLE
   op_by_block_1 = df[['block_1', 'op']].groupby('block_1', group_keys=False).op
   df['oracle_alive'] = op_by_block_1.shift(-1) == 'LD'
   
   # ADDR CNT: number of addresses accessed
   print(f'Address count trace: {tr}')
   comp_df['addr_cnt'] = count_once_per_interval(df, 'interval_7', 'block_1')
   comp_df['addr_cnt'].to_frame('total_addr_cnt').plot(kind='bar', figsize=(15,10))
   #plt.show()
   plt.savefig(f'./script_trace/plots/{tr}_addr_cnt_per_interval.png') 
   plt.close()
   
   # MODIFIED
   print(f'Modified trace: {tr}')
   by_interval = df[['block_1', 'op', 'interval_7', 'oracle_alive']].groupby('interval_7')
   comp_df['modified'] = count_st_once_per_interval(df, 'interval_7', 'block_1') 
   comp_df['modified'].to_frame('modified').plot(kind='bar', figsize=(15,10))
   #plt.show()
   plt.savefig(f'./script_trace/plots/{tr}_modified_in_interval.png') 
   plt.close()
   
   # ORACLE ALIVE
   print(f'Oracle alive trace: {tr}')
   comp_df['oracle_alive'] = by_interval.apply(count_last_use_alive)
   comp_df['oracle_alive'].to_frame('oracle_alive').plot(kind='bar', figsize=(15,10))
   #plt.show()
   plt.savefig(f'./script_trace/plots/{tr}_oracle_alive.png') 
   plt.close()
   
   # ORACLE & MODIFIED
   print(f'Oracle modified trace: {tr}')
   comp_df['oracle_modified'] = by_interval.apply(lambda g: g[g.block_1.isin(g[g.op == 'ST'].block_1) & g.loc[g.block_1.drop_duplicates(keep='last').index].oracle_alive].block_1.count())
   comp_df['oracle_modified'].to_frame('alive_and_modified').plot(kind='bar', figsize=(15,10))
   #plt.show()
   plt.savefig(f'./script_trace/plots/{tr}_oracle_modified.png') 
   plt.close()
   
   # HIBERNUS
   comp_df['hibernus_word'] =  len(df.block_1.unique())
   comp_df['hibernus_page_512'] = len(df.block_128.unique()) * 128
   comp_df['hibernus_page_1K'] = len(df.block_256.unique()) * 256
   comp_df['hibernus_page_2K'] = len(df.block_512.unique()) * 512

   for bs in block_sizes:
       comp_df[f'block_{bs}'] = count_once_per_interval(df, 'interval_7', f'block_{bs}') * bs 
   
   for bs in block_sizes:
       comp_df[f'block_{bs}_mod'] = count_st_once_per_interval(df, 'interval_7', f'block_{bs}') * bs 
   
   print(f'Writing file: ./script_trace/csvs/{tr}_strategy_comp.csv')
   comp_df.to_csv(f'./script_trace/csvs/{tr}_strategy_comp.csv')

def run():
    strategies = [
        'addr_cnt', 
        'modified', 
        'oracle_alive', 
        'oracle_modified'
    ]

    backup_size_avg_df = pd.DataFrame(columns=trace_files.keys(), index=strategies)
    
    block_sizes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    for tr in os.listdir('./script_traces/nocache') :
       analyse_trace(tr) 

if __name__ == '__main__':
    run()
