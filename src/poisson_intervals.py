import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from multiprocessing import Pool

import backup_strategies_eval as ta

col_names = ['cycle', 'op', 'width', 'addr']
block_sizes = [1, 8, 128]

def add_poisson_intervals(df, fail_rate=1e-7): 
    '''Add intervals considering power failures distributed as a poisson'''
    prog_cycles = df.cycle.iloc[-1] #df.cycle.max() 
    events = np.random.choice((0, 1), 
        size=prog_cycles, replace=True, p=[1-fail_rate, fail_rate])
    pwr_fail_times = np.where(events == 1)[0]
    df['interval'] = 0
    for t in pwr_fail_times:
        df['interval'] += t < df.cycle
    return len(pwr_fail_times)+1

def run_with_fail_rates(fail_rate): 
    

def main():
    np.random.seed(0)
    cols = ['full_mem_page512', 'savings_avg', 'savings_std', 'num_interval_avg', 'num_interval_std']
    comp_df = pd.DataFrame(columns=cols)
    fail_rates = [1/1e5, 1/5e5, 1/1e6, 1/5e6, 1/1e7] 

    trace_file = './script_traces/nocache/fft.csv' 
    df = pd.read_csv(trace_file, names=ta.col_names)

    ta.add_address_blocks(df, block_sizes)
    full_mem_page_512 = int(len(df.block_128.unique()) * 128)

     
    with Pool() as pool:
        for d in pool.map(run_with_fail_rate, fail_rates):
            comp_df.loc[d[0]] = d[1]
    comp_df.loc['average'] = comp_df.mean()
    print(comp_df)

    comp_df.to_csv('poisson_5e-6_iv_100.csv')
