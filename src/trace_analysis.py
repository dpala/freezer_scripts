import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

benchmarks = { 
    'benchmark' : ['fft_8_32', 'fft_8_32_cache', 'qsort_small','qsort_small_cache'],
    'tot_instr' : [6117015, 6117015, 18564366, 18564366],
    'tot_cycles' : [9445500, 24758005, 31228100, 101253977]
    }

col_names = ['cycle', 'op', 'width', 'addr']

trace_files = {
    #'fft_8_32' : './traces/fft_8_32.trace.csv',
    'qsort_small_cache' : './traces/qsort_small.cache.trace.csv',
    }

block_sizes = 2**np.arange(2,11) # 4 to 1024
interv_sizes = 10**np.arange(4,8) # 10k to 10M

################################################################
# Load data frames
################################################################

bench_df = pd.DataFrame(benchmarks,index=benchmarks['benchmark'])

dfs = { k: pd.read_csv(trace_files[k],names=col_names) for k in trace_files }

#for k in dfs:
#    dfs[k]['bench'] = k

df = dfs['qsort_small_cache']

df['addr'] = df.addr.apply(lambda x : int(x,16))

print(df.head())

################################################################
# Compute instructions per cycle IPC
################################################################
bench_df['IPC'] = bench_df['tot_instr'] / bench_df['tot_cycles'] 


################################################################
#
################################################################

f = df.groupby(['op','width']).size().to_frame(name='tot_op#') 
print(f)

 
#df.groupby(['op', 'width']).size().unstack().plot(kind='barh',subplots=False)
#plt.show()

blocks_stats = pd.DataFrame()

for bs in block_sizes:
    k = int(np.log2(bs))
    colname = f'block_{bs}'

    df[colname] = df.addr.apply(lambda x : hex((x>>k)<<k))

cols = [f'block_{bs}' for bs in block_sizes]

blocks_stats = pd.concat([pd.Series(df.groupby(col).op.size().describe(), name=col) for col in cols],axis=1) 
    
print(blocks_stats)
blocks_stats.loc['mean'].plot(kind='bar')
plt.show()