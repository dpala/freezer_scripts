import os
import numpy as np
import pandas as pd
from multiprocessing import Pool

import backup_strategies_eval as ta
import random_intervals_eval as riv


col_names = ['cycle', 'op', 'width', 'addr']
block_sizes = [1, 8, 128]


def run(name, times=100):
    np.random.seed(0)
    df = pd.read_csv(f'./script_traces/nocache/{name}', names=ta.col_names)

    ta.add_address_blocks(df, block_sizes)
    mem_page_512 = int(len(df.block_128.unique()) * 128)

    ops = df.op.value_counts()  # give back number of LD and ST
    # pwr_fail_per_cycle = 1e-6
    # prog_cycles = df.cycle.iloc[-1]
    E_sram_w, E_sram_r = 1, 1
    E_nvm_w = 10 * E_sram_w
    E_nvm_r = 1 * E_sram_r
    E_sram_prog = E_sram_w * ops['ST'] + E_sram_r * ops['LD']
    E_save = E_sram_r + E_nvm_w
    E_restore = mem_page_512 * (E_nvm_r + E_sram_w)
    E_full_backup = mem_page_512 * E_save
    E_run_restore = E_sram_prog + E_restore

    E_nvm_tot = E_nvm_w * ops['ST'] + E_nvm_r * ops['LD']

    energies_frezer = []
    energies_full_mem = []

    df['interval'] = 0
    for i in range(times):
        n = riv.add_poisson_intervals(df)
        # n = np.random.poisson(pwr_fail_per_cycle * prog_cycles)
        # pwr_fail_times = np.cumsum(np.random.geometric(pwr_fail_per_cycle, n)
        # for t in pwr_fail_times:
        #     if t > prog_cycles:
        #         break
        #     df['interval'] += t < df.cycle
        # n = df.interval.nunique() # number of intervals
        modified = ta.count_st_once_per_interval(df, 'interval', 'block_8') * 8
        E_freezer_tot = E_run_restore + E_save * modified.sum()
        E_full_mem_tot = E_run_restore + n * E_full_backup
        energies_frezer.append(E_freezer_tot)
        energies_full_mem.append(E_full_mem_tot)

    res = {
        'freezer_avg': np.mean(energies_frezer),
        'freezer_std': np.std(energies_frezer),
        'full_mem_backup_avg': np.mean(energies_full_mem),
        'full_mem_backup_std': np.std(energies_full_mem),
        'NVM': E_nvm_tot,
    }
    return name, res


def main():
    cols = ['freezer_avg', 'freezer_std', 'full_mem_backup_avg', 'full_mem_backup_std', 'NVM']
    comp_df = pd.DataFrame(columns=cols)

    with Pool() as pool:
        for d in pool.map(run, os.listdir('./script_traces/nocache/')):
            comp_df.loc[d[0]] = d[1]
    comp_df.loc['average'] = comp_df.mean()
    print(comp_df)

    comp_df.to_csv('energy_10nvm_1e-6iv_100_poisson.csv')


if __name__ == '__main__':
    main()
