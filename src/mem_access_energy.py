import os
import numpy as np
import pandas as pd
from multiprocessing import Pool
from functools import partial

from pint import UnitRegistry

import backup_strategies_eval as ta


def cache_energy(cache, cache_trace_df, nvm, mem_size, n_intevals):
    '''compute the cache energy dividing it into tree components
    (hits, misses and flushes).'''

    E_miss = cache['Miss Energy']
    E_hit = cache['Hit Energy']
    E_cache_w = cache['Write Energy']
    # block size in bytes divided by 4 bytes/word
    words_per_block = cache['Block Size'] // 4

    E_nvm_r = nvm.loc[mem_size]['Read Energy']
    E_nvm_w = nvm.loc[mem_size]['Write Energy']

    N_lines = cache['Size'] // (cache['Block Size'] * cache['Associativity'])
    N_hits_r = cache_trace_df.read_hits.sum()
    N_hits_w = cache_trace_df.write_hits.sum()
    # N_hits = N_hits_r + N_hits_w
    N_evicts = cache_trace_df.evicts.sum()
    N_flushes = cache_trace_df.flushes.sum()
    N_misses = sum(cache_trace_df.read_misses + cache_trace_df.write_misses)

    E_hits = E_hit * N_hits_r + (E_hit + E_cache_w) * N_hits_w
    # On miss, read block from NVM and write it into the cache
    # In case of conflict evict conflicting block an write it in NVM
    E_misses = (E_miss + E_nvm_r * words_per_block + E_cache_w) * N_misses + \
        N_evicts * E_nvm_w * words_per_block
    # read all the lines and write to NVM the dirty ones
    E_flushes = n_intevals * E_hit * N_lines + \
        E_nvm_w * N_flushes * words_per_block

    # E_total_nvm_w = N_evicts * E_nvm_w * words_per_block
    #               + N_evicts * E_nvm_w * words_per_block
    # E_total_nvm_r = E_nvm_r * words_per_block * N_misses
    # E_cache_miss = (E_miss + E_cache_w) * N_misses

    return E_hits, E_misses, E_flushes


def run(name, freq, sram_lut, nvms, caches):
    # cache_cols = 'interval evicts flushes hits mem_wr misses stores'.split(' ')
    block_sizes = [1, 8]
    df = pd.read_csv(f'../traces/{name}', names=ta.col_names)

    ta.add_address_blocks(df, block_sizes)
    mem_pwr_2 = int(2 ** np.ceil(np.log2(df.block_1.nunique())))

    mem_size = int(mem_pwr_2 * 4)  # memory size in bytes

    if mem_size not in sram_lut.index:
        print(f'[{name}]: memory size ({mem_size}) not in [{sram_lut.index}]')
        return None

    ops = df.op.value_counts()  # give back number of LD and ST
    pwr_fail_per_cycle = 1e-6
    prog_cycles = df.cycle.iloc[-1]

    E_sram_r = sram_lut.loc[mem_size]['Read Energy']
    E_sram_w = sram_lut.loc[mem_size]['Write Energy']
    P_sram_leak = sram_lut.loc[mem_size]['Leakage']

    E_sram_ld = E_sram_r * ops['LD']
    E_sram_st = E_sram_w * ops['ST']
    E_sram_prog = E_sram_ld + E_sram_st

    df['interval'] = np.array(np.round(df.cycle * pwr_fail_per_cycle), dtype=int)
    n = df.interval.nunique()  # number of intervals

    modified_in_interval = ta.count_st_once_per_interval(df, 'interval', 'block_8') * 8

    res = {
        'freezer_avg_backup_size': modified_in_interval.mean(),
        'freezer_tot_backup_size': modified_in_interval.sum(),
        'num_intervals': n,
        'mem_size': int(mem_pwr_2),
        'E_program': E_sram_prog,
        'E_LD': E_sram_ld,
        'E_ST': E_sram_st,
        'E_leakage_sram': P_sram_leak * prog_cycles / freq,
    }

    for nvm, lut in nvms.items():
        E_nvm_r = lut.loc[mem_size]['Read Energy']
        E_nvm_w = lut.loc[mem_size]['Write Energy']
        P_nvm_leak = lut.loc[mem_size]['Leakage']

        E_save = E_sram_r + E_nvm_w
        E_restore = mem_pwr_2 * (E_nvm_r + E_sram_w)
        E_full_backup = mem_pwr_2 * E_save

        E_nvm_tot = E_nvm_w * ops['ST'] + E_nvm_r * ops['LD']
        E_freezer_restore = E_restore * n
        E_freezer_save = E_save * modified_in_interval.sum()
        E_freezer_tot = E_sram_prog + E_freezer_restore + E_freezer_save
        E_full_mem_tot = E_sram_prog + n * (E_restore + E_full_backup)

        for size, cache in caches.items():
            cache_df = pd.read_csv(f'../cache_res/cache{size}/{name}')
            cache['Size'] = int(size)
            E_hits, E_misses, E_flushes = cache_energy(cache, cache_df, lut, mem_size, n)
            res[f'E_cache_hits_{size}_{nvm}'] = E_hits
            res[f'E_cache_misses_{size}_{nvm}'] = E_misses
            res[f'E_cache_flushes_{size}_{nvm}'] = E_flushes
            res[f'E_cache{size}_{nvm}'] = E_hits + E_misses + E_flushes

        res[f'E_freezer_{nvm}'] = E_freezer_tot
        res[f'E_freezer_restore_{nvm}'] = E_freezer_restore
        res[f'E_freezer_save_{nvm}'] = E_freezer_save
        res[f'E_full_memory_{nvm}'] = E_full_mem_tot
        res[f'E_leakage_{nvm}'] = P_nvm_leak * prog_cycles / freq
        res[f'E_{nvm}_only'] = E_nvm_tot

    return name, res


def build_luts():
    ureg = UnitRegistry()
    Q_ = ureg.Quantity
    tmp = pd.read_csv('../nvsim_res/sram_st_c28soi.csv', skipinitialspace=True)
    sram_lut = tmp[['Capacity', 'Read Latency', 'Write Latency', 'Read Energy', 'Write Energy', 'Leakage']]
    sram_lut.loc[:, 'Read Energy'] = sram_lut['Read Energy'].apply(lambda e: Q_(e).to('pJ').magnitude)
    sram_lut.loc[:, 'Write Energy'] = sram_lut['Write Energy'].apply(lambda e: Q_(e).to('pJ').magnitude)
    sram_lut.loc[:, 'Read Latency'] = sram_lut['Read Latency'].apply(lambda e: Q_(e).to('ns').magnitude)
    sram_lut.loc[:, 'Write Latency'] = sram_lut['Write Latency'].apply(lambda e: Q_(e).to('ns').magnitude)
    sram_lut.loc[:, 'Leakage'] = sram_lut['Leakage'].apply(lambda e: Q_(e).to('uW').magnitude)
    #sram_lut['Leakage'] = {2048: 0.62, 4096: 0.78, 8192: 1.1, 16384: 2.16, 32768: 3.58}
    sram_lut.loc[:, 'Capacity'] = sram_lut.Capacity.apply(lambda e: int(Q_(e).to('B').magnitude))
    sram_lut.set_index('Capacity', inplace=True)
    #print(sram_lut[['Read Latency', 'Write Latency', 'Read Energy', 'Write Energy']].applymap(Q_))
    nvms = {
        'stt': pd.read_csv('../nvsim_res/stt.csv', skipinitialspace=True),
        'stt_aggressive': pd.read_csv('../nvsim_res/stt_aggressive.csv', skipinitialspace=True),
        'rram': pd.read_csv('../nvsim_res/rram.csv', skipinitialspace=True),
    }

    for nvm, tmp in nvms.items():
        df = tmp[['Capacity', 'Read Latency', 'Write Latency', 'Read Energy', 'Write Energy', 'Leakage']]
        df.loc[:, 'Read Energy'] = df['Read Energy'].apply(lambda e: Q_(e).to('pJ').magnitude)
        df.loc[:, 'Write Energy'] = df['Write Energy'].apply(lambda e: Q_(e).to('pJ').magnitude)
        df.loc[:, 'Read Latency'] = df['Read Latency'].apply(lambda e: Q_(e).to('ns').magnitude)
        df.loc[:, 'Write Latency'] = df['Write Latency'].apply(lambda e: Q_(e).to('ns').magnitude)
        df.loc[:, 'Leakage'] = df['Leakage'].apply(lambda e: Q_(e).to('uW').magnitude)
        df.loc[:, 'Capacity'] = df.Capacity.apply(lambda e: int(Q_(e).to('B').magnitude))
        df.set_index('Capacity', inplace=True)
        nvms[nvm] = df

    caches = {
        '2048': {'Miss Energy': '0.005nJ', 'Hit Energy':  '0.005nJ', 'Write Energy': '0.004nJ', 'Block Size': 32, 'Associativity': 4},
        '4096': {'Miss Energy': '0.006nJ', 'Hit Energy':  '0.006nJ', 'Write Energy': '0.004nJ', 'Block Size': 32, 'Associativity': 4},
        '8192': {'Miss Energy': '0.01nJ', 'Hit Energy':  '0.01nJ', 'Write Energy': '0.009nJ', 'Block Size': 32, 'Associativity': 4},
        '16384': {'Miss Energy': '0.013nJ', 'Hit Energy':  '0.013nJ', 'Write Energy': '0.013nJ', 'Block Size': 32, 'Associativity': 4},
    }

    for size in caches:
        caches[size]['Miss Energy'] = Q_(caches[size]['Miss Energy']).to('pJ').magnitude
        caches[size]['Hit Energy'] = Q_(caches[size]['Hit Energy']).to('pJ').magnitude
        caches[size]['Write Energy'] = Q_(caches[size]['Write Energy']).to('pJ').magnitude

    return sram_lut, nvms, caches


def main():
    sram_lut, nvms, caches = build_luts()
    comp_df = None
    run_fn = partial(run, freq=20, sram_lut=sram_lut, nvms=nvms, caches=caches)
    # d = run_fn('fft.csv')
    # if d is not None:
    #     comp_df = pd.DataFrame(d[1], index=[d[0]])
    # print(d[1])
    with Pool() as pool:
        for d in pool.map(run_fn, os.listdir('../traces/')):
            if d is None:
                continue
            if comp_df is None:
                comp_df = pd.DataFrame(d[1], index=[d[0]])
            else:
                comp_df.loc[d[0]] = d[1]
    comp_df.mem_size = np.int32(comp_df.mem_size)
    comp_df.loc['average'] = comp_df.mean()
    print(comp_df)

    comp_df.to_csv('energy_1e-6iv.csv')


if __name__ == '__main__':
    main()
